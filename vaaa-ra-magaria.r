df <- swiss

#1
df$Category <- cut(df$Education, breaks=c(0, 4, 7, 10, 53), labels=c("A", "B", "C", "D"), include.lowest = TRUE)

df$ExamCategory <- cut(df$Examination, breaks = c(0, 7, 20, 37), labels = c("k", "m", "n"))

min(df[df$Category=="A" & df$ExamCategory=="k", ]$Fertility)

#2
df$Sex <- cut(df$Catholic, breaks = c(0, 10, 90,  100), labels = c("M", "F", "M") )
mean(df[df$Category=="A" & df$Sex=="M", ]$Fertility) - mean(df[df$ExamCategory=="m" & df$Sex=="F", ]$Fertility)

#3
respublicans <- df[df$ExamCategory=="k" & df$Sex=="M", ]
nrow(respublicans[respublicans$Infant.Mortality < 18, ])/nrow(respublicans) * 100

#4
asd <- df[df$ExamCategory=="m" & df$Catholic < 50, ]
nrow(asd[asd$Examination < 15, ])/nrow(asd) * 100

#8
x <- df$Fertility
y <- df$Agriculture
colors <- c("red", "green", "blue", "purple")

plot(x, y,type = "p",main = "asd",xlab = "kruta", ylab = "nekruta", pch=16, cex=2, col =  colors[match(df$Category, c("A", "B", "C", "D"))])
legend("topleft", legend = c("A", "b", "C", "D"), col= colors, pch = 16)

#9
repeats = 10000

witeli <- 1/2
jvari <- 1/4
yvavi <- 12/52
tuzi <- 1/52
wins = c(0, 5, 10, 30)
s = 0

for (i in 1:repeats) {
  card <- sample(wins, 1, replace = FALSE, prob = c(witeli, jvari, yvavi, tuzi))
  s = s + card
}
s/repeats

#10
s = 0

for (i in 1:repeats) {
  dice <- sum(sample(1:6, 2, replace = TRUE))
  if(dice < 10){
    s = s + 1
  }
}
s/repeats
5/6

#11
s=0
k=0

for (i in 1:repeats) {
  dice1 <- sample(1:6, 1, replace = FALSE)
  dice2 <- sample(1:6, 1, replace = FALSE)
  if(dice1+dice2>10){
    s = s + 1
    if(dice1==5 | dice2==5){
      k = k + 1
    }
  }
}

k/s
2/3

#12
larebi <- 0

for (i in 1:repeats){
  dice <- sample(1:6, 4, replace = TRUE)
  if(any(dice==6)){
    larebi <- larebi + 1
  }
}

larebi
